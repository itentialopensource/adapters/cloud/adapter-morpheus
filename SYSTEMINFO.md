# Morpheus

Vendor: Morpheus
Homepage: https://morpheusdata.com/

Product: Morpheus
Product Page: https://morpheusdata.com/

## Introduction
We classify Morpheus into the Cloud domain as Morpheus provides a solution for managing and automating cloud infrastructure and applications.

"Morpheus is a powerful cloud management tool that provides provisioning, monitoring, logging, backups, and application deployment strategies."

## Why Integrate
The Morpheus adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Morpheus. With this adapter you have the ability to perform operations on items such as:

- Network
- Clusters
- Instances

## Additional Product Documentation
[Morpheus API Documentation](https://apidocs.morpheusdata.com/reference/)