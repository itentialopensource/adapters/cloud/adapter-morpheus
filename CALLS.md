## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Morpheus. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Morpheus.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Morpheus. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createCluster(body, callback)</td>
    <td style="padding:15px">create a cluster</td>
    <td style="padding:15px">{base_path}/{version}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusters(filter, callback)</td>
    <td style="padding:15px">get all clusters</td>
    <td style="padding:15px">{base_path}/{version}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterById(clusterId, callback)</td>
    <td style="padding:15px">get cluster by id</td>
    <td style="padding:15px">{base_path}/{version}/cluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCluster(clusterId, body, callback)</td>
    <td style="padding:15px">update a cluster</td>
    <td style="padding:15px">{base_path}/{version}/cluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCluster(clusterId, options, callback)</td>
    <td style="padding:15px">delete cluster</td>
    <td style="padding:15px">{base_path}/{version}/cluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateClusterPermissions(clusterId, body, callback)</td>
    <td style="padding:15px">update cluster permissions</td>
    <td style="padding:15px">{base_path}/{version}/cluster/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainers(clusterId, filter, callback)</td>
    <td style="padding:15px">get containters</td>
    <td style="padding:15px">{base_path}/{version}/cluster/{pathv1}/containers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInstance(body, callback)</td>
    <td style="padding:15px">create an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstances(filter, callback)</td>
    <td style="padding:15px">get all instances</td>
    <td style="padding:15px">{base_path}/{version}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceById(instanceId, callback)</td>
    <td style="padding:15px">get instance by id</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInstance(instanceId, body, callback)</td>
    <td style="padding:15px">update a instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInstance(instanceId, options, callback)</td>
    <td style="padding:15px">delete instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceEnvironment(instanceId, callback)</td>
    <td style="padding:15px">get instance environment variables</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/envs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceHistory(instanceId, callback)</td>
    <td style="padding:15px">get instance history</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceContainerDetails(instanceId, callback)</td>
    <td style="padding:15px">get instance container details</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/containers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceTypes(callback)</td>
    <td style="padding:15px">get all instance types</td>
    <td style="padding:15px">{base_path}/{version}/instance-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceTypeById(typeId, callback)</td>
    <td style="padding:15px">get instance type by id</td>
    <td style="padding:15px">{base_path}/{version}/instance-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicePlans(filter, callback)</td>
    <td style="padding:15px">get available service plans</td>
    <td style="padding:15px">{base_path}/{version}/instances/service-plans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopInstance(instanceId, callback)</td>
    <td style="padding:15px">stop an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startInstance(instanceId, callback)</td>
    <td style="padding:15px">start an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartInstance(instanceId, callback)</td>
    <td style="padding:15px">restart an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">suspendInstance(instanceId, callback)</td>
    <td style="padding:15px">suspend an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rejectInstance(instanceId, callback)</td>
    <td style="padding:15px">reject an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/reject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resizeInstance(instanceId, body, callback)</td>
    <td style="padding:15px">resize an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/resize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runInstanceWorkflow(instanceId, workflowId, workflowName, body, callback)</td>
    <td style="padding:15px">run workflow on an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneInstance(instanceId, body, callback)</td>
    <td style="padding:15px">clone an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupInstance(instanceId, callback)</td>
    <td style="padding:15px">backup an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInstanceBackups(instanceId, callback)</td>
    <td style="padding:15px">get list of backups for an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInstanceSnapshots(instanceId, callback)</td>
    <td style="padding:15px">get list of snapshots for an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">snapshotInstance(instanceId, body, callback)</td>
    <td style="padding:15px">snapshot an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/snapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importInstanceSnapshot(instanceId, body, callback)</td>
    <td style="padding:15px">import instance snapshot</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/import-snapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneInstanceImage(instanceId, body, callback)</td>
    <td style="padding:15px">clone instance to image</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/clone-image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lockInstance(instanceId, callback)</td>
    <td style="padding:15px">lock an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlockInstance(instanceId, callback)</td>
    <td style="padding:15px">unlock an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/unlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInstanceSecurityGroups(instanceId, callback)</td>
    <td style="padding:15px">get security groups for an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/security-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setInstanceSecurityGroups(instanceId, body, callback)</td>
    <td style="padding:15px">set security groups for an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/security-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelDeleteInstance(instanceId, callback)</td>
    <td style="padding:15px">cancel delete of an instance</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/cancel-removal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidents(filter, callback)</td>
    <td style="padding:15px">get all incidents</td>
    <td style="padding:15px">{base_path}/{version}/monitoring/incidents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetwork(body, callback)</td>
    <td style="padding:15px">create a network</td>
    <td style="padding:15px">{base_path}/{version}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworks(filter, callback)</td>
    <td style="padding:15px">get all networks</td>
    <td style="padding:15px">{base_path}/{version}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkById(networkId, callback)</td>
    <td style="padding:15px">get network by id</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetwork(networkId, body, callback)</td>
    <td style="padding:15px">update a network</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetwork(networkId, callback)</td>
    <td style="padding:15px">delete network</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkTypes(filter, callback)</td>
    <td style="padding:15px">get all network types</td>
    <td style="padding:15px">{base_path}/{version}/network-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkTypeById(typeId, callback)</td>
    <td style="padding:15px">get network type by id</td>
    <td style="padding:15px">{base_path}/{version}/network-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkGroup(body, callback)</td>
    <td style="padding:15px">create a network grouop</td>
    <td style="padding:15px">{base_path}/{version}/networks/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGroups(filter, callback)</td>
    <td style="padding:15px">get all network groups</td>
    <td style="padding:15px">{base_path}/{version}/networks/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGroupById(groupId, callback)</td>
    <td style="padding:15px">get network group by id</td>
    <td style="padding:15px">{base_path}/{version}/networks/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGroup(groupId, body, callback)</td>
    <td style="padding:15px">update a network group</td>
    <td style="padding:15px">{base_path}/{version}/networks/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGroup(groupId, callback)</td>
    <td style="padding:15px">delete network group</td>
    <td style="padding:15px">{base_path}/{version}/networks/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpecificCatalogInventoryItem(id, callback)</td>
    <td style="padding:15px">getSpecificCatalogInventoryItem</td>
    <td style="padding:15px">{base_path}/{version}/api/catalog/items/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCatalogCart(callback)</td>
    <td style="padding:15px">getCatalogCart</td>
    <td style="padding:15px">{base_path}/{version}/api/catalog/cart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeCatalogItem(id, body, callback)</td>
    <td style="padding:15px">removeCatalogItem</td>
    <td style="padding:15px">{base_path}/{version}/api/catalog/cart/items/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkoutCatalogCart(body, callback)</td>
    <td style="padding:15px">checkoutCatalogCart</td>
    <td style="padding:15px">{base_path}/{version}/api/catalog/checkout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCatalogInventoryItems(max, offset, sort, direction, phrase, name, callback)</td>
    <td style="padding:15px">listCatalogInventoryItems</td>
    <td style="padding:15px">{base_path}/{version}/api/catalog/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addItemToCart(validate, body, callback)</td>
    <td style="padding:15px">addItemToCart</td>
    <td style="padding:15px">{base_path}/{version}/api/catalog/cart/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
