
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:33PM

See merge request itentialopensource/adapters/adapter-morpheus!15

---

## 0.5.3 [09-05-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-morpheus!13

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_18:47PM

See merge request itentialopensource/adapters/adapter-morpheus!12

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_20:00PM

See merge request itentialopensource/adapters/adapter-morpheus!11

---

## 0.5.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-morpheus!10

---

## 0.4.5 [03-28-2024]

* Changes made at 2024.03.28_13:18PM

See merge request itentialopensource/adapters/cloud/adapter-morpheus!8

---

## 0.4.4 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-morpheus!7

---

## 0.4.3 [03-11-2024]

* Changes made at 2024.03.11_15:33PM

See merge request itentialopensource/adapters/cloud/adapter-morpheus!6

---

## 0.4.2 [02-28-2024]

* Changes made at 2024.02.28_11:46AM

See merge request itentialopensource/adapters/cloud/adapter-morpheus!5

---

## 0.4.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-morpheus!4

---

## 0.4.0 [12-18-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-morpheus!3

---

## 0.3.0 [10-06-2022]

* ADAPT-2379 Update Morpheus adapter with six calls

See merge request itentialopensource/adapters/cloud/adapter-morpheus!2

---

## 0.2.0 [05-20-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-morpheus!1

---

## 0.1.1 [04-22-2021]

- Initial Commit

See commit 806e66c

---
