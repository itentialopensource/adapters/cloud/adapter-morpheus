# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Morpheus System. The API that was used to build the adapter for Morpheus is usually available in the report directory of this adapter. The adapter utilizes the Morpheus API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Morpheus adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Morpheus. With this adapter you have the ability to perform operations on items such as:

- Network
- Clusters
- Instances

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
